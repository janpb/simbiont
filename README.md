# Simple Bioinformatic Tools

## Abstract

`Simbiont` aims to be a collections of small, but not necessary simple,
tools to handle bioinformatic data, either by acting as a translator between
different tasks in pipelines or doing one specific task really good. The idea
behind `Simbiont` is to try an implementation of the *Unix Philosophy* in
bioinformatics by using specific libraries for specific tasks. While several
tools and libraries exists to perform more complex biological analysis, e.g.
[biopython](http://biopython.org), they can sometimes be an overkill for
data preparation. Data preparation, e.g. filtering or parsing, is something of
a household chore and everybody uses mostly own tools to accomplish such tasks.
`Simbiont` facilitates the implementation of such tools by using (hopefully)
easy-to-use libraries and offers ready-to-use tools for the most common tasks,
e.g. parse BLAST outputs. In addition, numerous tools and packages to analyze
biological data exist, so why not add one more.

`Simbiont` is currently being developed by me as a postdoc in
Prof. Edward C. Holmes' at the University of Sydney.

* [Website](http://sydney.edu.au/science/biology/viralevolution/index.shtml)
* [GitHub](https://github.sydney.edu.au/virusevolution)

The `gitlab` repository is mirrored to
[this GitHub repository](https://github.sydney.edu.au/virusevolution/simbiont.git)

### Important
Don't hesitate to contact me or open an issue on at
https://gitlab.com/janpb/simbiont if you get contacted.

## Status

### 2018-02-21

Adding initial callimachus functionality, look out for bugs.

### 2018-02-01
  Simbiont is currently being overhauled to accommodate `blib`. Partially broken
  and limited number of tools.

  **__Issues are welcome__**

## Dependencies

Simbiont tools rely on the standard Python 3 libraries and the
[`blib library`](https://gitlab.com/janpb/blib). `blib` is the only external
library and is being developed by me. 'blib' is included as a `git submodule` 
and has a dedicated `simbiont` branch.


## Installation / Updating
`$SIMBIONT` indicates the path where you clone the repository, e.g. if you
cloned the repository in the directory '$HOME/tools', `$SIMBIONT` will be
`$HOME/tools/simbiont`

* Clone the repository:
    * `cd to/the/directory/to/clone/simbiont/into`
    * `git clone --recursive https://gitlab.com/janpb/simbiont.git`

* Update:
  * `cd $SIMBIONT`
  * `git pull`
  * `git submodule update --recursive --remote`

### Currently available tools
`$SIMBIONT` indicates the path to the cloned repository.

| Topic | Location             |Name                     | Description                                      |
|-------|----------------------|-------------------------|--------------------------------------------------|
| NCBI  | `$SIMBIONT/ncbi/src` | `efetch.py`             | Fetch data via NCBI's EDirect server             |
|       |                      | `esearch.py`            | Search NCBI's EDirect server                     |
|       |                      | `ncbi.taxonomist.py`    | Convert NCBI taxid's into lineages               |
|       |                      | `callimachus_pubmed.py` | Fetch Pubmed articles for NCBI accessions or UIDs|

ncbi/src/
#### Invoke tools

- `$SIMBIONT/ncbi/src/ncbi.taxonomist.py`

- try the `-h` argument, e.g. `$SIMBIONT/ncbi/src/ncbi.taxonomist.py -h`

#### Note for NCBI tools

  * Bugs

    - REST requests connect ti NCBI. If an error occurs, test if it's a
      connection issue, e.g. just rerun the command and see if it still fails.

    - Open an issue: paste your exact command and the error message.

  * Email address

    Interacting with NCBI EDirect servers requires your email address as an
    argument. NCBI monitors how a tool behaves and could contact you if it uses
    too much resources or similar.
    While I don't know how you use the tool, don't hesitate to contact me or
    open an issue on at https://gitlab.com/janpb/simbiont if you get contacted.

  * API Key

    NCBI introduced
    [API keys](https://ncbiinsights.ncbi.nlm.nih.gov/2017/11/02/new-api-keys-for-the-e-utilities/),
    allowing 10 requests per second.

    * The tools check if you have an NCBI API  key by checking for the
       environment variable`NCBI_API_KEY`.
    * You can pass your API key as an argument.

### Remarks
- Serious lack of documentation. Most tools have an USAGE which can be
  invoked using `-h`, but that's all.

- Bugs, so be aware and check results after first usage.
